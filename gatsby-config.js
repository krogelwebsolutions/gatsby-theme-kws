require('dotenv').config()
module.exports = options => {
  const {
    contentPath,
    assetsPath,
    siteMetadata = {},
    manifest = {},
    layout = './src/lib/Layout/Layout.js',
    removeConsole = true,
  } = options

  return {
    siteMetadata: {
      title: 'Krogel Web Solutions',
      description: 'Krogel Web Solutions',
      menuLinks: [
        {
          name: 'Home',
          to: '/',
        },
      ],
      ...siteMetadata,
    },
    plugins: [
      '@bumped-inc/gatsby-plugin-optional-chaining', //TODO:https://github.com/gatsbyjs/gatsby/pull/19302/files
      'gatsby-plugin-react-helmet',
      {
        //to create the theme pages. site pages re created automatically
        resolve: 'gatsby-plugin-page-creator',
        options: {
          path: `${__dirname}/src/pages`,
        },
      },
      {
        resolve: 'gatsby-source-filesystem',
        options: {
          path: contentPath || `content`,
          name: contentPath || `content`,
        },
      },
      {
        resolve: 'gatsby-source-filesystem',
        options: {
          path: assetsPath || `assets`,
          name: assetsPath || `assets`,
        },
      },
      {
        resolve: 'gatsby-source-filesystem',
        options: {
          path: `${__dirname}/assets`,
          name: `theme-assets`,
        },
      },
      `gatsby-transformer-json`, //probably not needed in base theme. include it at site-level
      {
        resolve: `gatsby-plugin-sharp`,
        options: {
          useMozJpeg: false, //better compression, increases build time
          stripMetadata: true,
          defaultQuality: 75,
        },
      },
      'gatsby-transformer-sharp',
      //was causing problems with third party sliders

      // {
      //   resolve: 'gatsby-plugin-purgecss', // purges all unused/unreferenced css rules
      //   options: {
      //     tailwind: false,
      //     develop: true, // Activates purging in npm run develop
      //     // purgeOnly: ['src/style.css'], // applies purging only on the bulma css file
      //   },
      // }, // must be after other CSS plugins
      {
        resolve: `gatsby-plugin-layout`,
        options: {
          component: require.resolve(layout),
        },
      },
      // 'gatsby-plugin-emotion',
      'gatsby-plugin-theme-ui',
      // 'gatsby-plugin-remove-console',
      removeConsole && 'gatsby-plugin-remove-console',
      {
        resolve: `gatsby-plugin-manifest`,
        options: {
          name: 'Krogel Web Solutions',
          short_name: 'KWS', //TODO:try to pull from single config
          start_url: `/`,
          background_color: '#663399',
          theme_color: '#663399',
          display: 'minimal-ui', //standalone? what does this do
          icon: require.resolve('./assets/favicon.png'), ///src/images/favicon.png'
          ...manifest,
        },
      },
      'gatsby-plugin-netlify', // make sure to keep it last in the array
      // `gatsby-plugin-offline`,
    ].filter(Boolean),
    //gatsby-plugin-netlify-functions
  }
}
