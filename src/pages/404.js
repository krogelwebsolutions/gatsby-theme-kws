import React from 'react'
import { graphql } from 'gatsby'
import { NotFoundPageTemplate } from 'lib/NotFoundPage'

const NotFoundPage = ({ data }) => {
  return (
    // <NotFoundPageTemplate {...notFoundJson} image={image.childImageSharp} />
    <NotFoundPageTemplate
      heading="asd"
      text="asd"
      image={data.image.childImageSharp}
    />
  )
}

export default NotFoundPage
// notFoundJson {
//     heading
//     text
// }
export const NotFoundPageQuery = graphql`
  query NotFoundPage {
    image: file(
      sourceInstanceName: { eq: "theme-assets" }
      name: { eq: "bernard" }
    ) {
      childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`
