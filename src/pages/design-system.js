import React from 'react'
import { DesignSystemPreview } from 'lib'

//TODO: use gatsby config to drive whether or not to generate this page or not
const DesignSystemPage = () => {
  return (
    // <NotFoundPageTemplate {...notFoundJson} image={image.childImageSharp} />
    <DesignSystemPreview />
  )
}

export default DesignSystemPage
