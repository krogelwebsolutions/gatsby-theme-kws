import { GoChevronDown } from 'react-icons/go' //scroll down animated chevron (scroll cue)
import { FaLongArrowAltRight, FaSistrix, FaWindowClose } from 'react-icons/fa'

//import icons from react-icons and give them a key
export default {
  close: FaWindowClose,
  'scroll-cue': GoChevronDown,
  'arrow-right': FaLongArrowAltRight,
  'zoom-in': FaSistrix,
}
