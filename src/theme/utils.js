import { get as themeGet } from '@styled-system/css'
import { transparentize } from 'polished' //TODO:switch to @theme-ui/color

//temporary work around for transparentize w/ theme-ui css vars until they add it to @theme-ui/colors
//https://github.com/system-ui/theme-ui/blob/master/packages/color/src/index.js
const g = (t, c) =>
  themeGet(t, `colors.${c}`, c)
    .replace(/^var\(--(\w+)(.*?), /, '')
    .replace(/\)/, '')

export const get = g
export const alpha = (color, amount) => t => transparentize(amount, g(t, color))

export const palxToChakra = palx => {
  const chakra = {}
  for (let [key, value] of Object.entries(palx)) {
    if (typeof value !== 'string') {
      // console.log(key, value)
      //convert array to object
      const c = {
        50: value[0],
        100: value[1],
        200: value[2],
        300: value[3],
        400: value[4],
        500: value[5],
        600: value[6],
        700: value[7],
        800: value[8],
        900: value[9],
      }

      chakra[key] = c
    } else {
      chakra[key] = value
    }
  }
  return chakra
}
