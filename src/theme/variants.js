import { styles as button } from 'lib/Button'
import { styles as icon } from 'lib/Icon'
import { styles as text } from 'lib/Text'
import { styles as card } from 'lib/Card'
import { styles as formControl } from 'lib/FormControl'
import { styles as layout } from 'lib/Layout'
import { styles as gallery } from 'lib/Gallery'
import { styles as modal } from 'lib/Modal'
import { styles as input } from 'lib/Input'
export default {
  button,
  formControl,
  text,
  card,
  layout,
  icon,
  gallery,
  modal,
  input,
}
