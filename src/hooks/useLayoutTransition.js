import { useContext } from 'react'
// import TransitionContext from 'lib/Layout/TransitionContext'
import { useTracked } from 'lib/Layout/TransitionContext/TransitionContext'
console.log(useTracked, 'useTracked')

//useTransitionContext may be more appropriate name
// const useLayoutTransition = () => {
//   return useContext(TransitionContext)
// }
const useLayoutTransition = () => {
  return useTracked()
}
export default useLayoutTransition
//TODO:remove this or update to correct imports/exports
