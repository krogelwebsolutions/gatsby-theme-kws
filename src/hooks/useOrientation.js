import { useState, useEffect, useCallback } from 'react'

const STATE_PORTRAIT = {
  orientation: 'portrait',
  isPortrait: true,
  isLandscape: false,
}
const STATE_LANDSCAPE = {
  orientation: 'landscape',
  isPortrait: false,
  isLandscape: true,
}

const useOrientation = (defaultState = STATE_PORTRAIT) => {
  const getOrientation = useCallback(() => {
    if (typeof window === 'undefined') {
      return defaultState
    }

    const portrait = window.matchMedia('(orientation: portrait)')
    // console.log(
    //   'orientation change**',
    //   portrait.matches ? 'portrait' : 'landscape'
    // )

    return portrait.matches ? STATE_PORTRAIT : STATE_LANDSCAPE

    return { orientation, isPortrait, isLandscape }
  }, [defaultState])

  const [value, setValue] = useState(getOrientation)
  //TODO:should this be usecallback? or method use-viewport-sizes uses?
  const onResize = () => {
    const newValue = getOrientation()
    console.log('orientation change', newValue)

    if (value !== newValue) {
      setValue(newValue)
    }
  }
  useEffect(() => {
    // const onResize = () => {
    //   const newValue = getOrientation()
    //   // console.log('orientation change', newValue)

    //   if (value !== newValue) {
    //     setValue(newValue)
    //   }
    // }
    window.addEventListener('resize', onResize)
    return () => window.removeEventListener('resize', onResize)
  }, [getOrientation, value])

  return [value, onResize]
}
//TODO:return {orientation, isPortait, isLandscape, angle, updateOrientation}
export default useOrientation
