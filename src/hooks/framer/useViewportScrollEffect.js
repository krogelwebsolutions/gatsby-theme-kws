import { useSpring, useTransform, useViewportScroll } from 'framer-motion'

//probably rename to useViewportScrollEffect since it is dependend on that
const useScrollEffect = (
  input,
  output,
  springConfig = {
    damping: 100,
    stiffness: 100,
    mass: 1,
  }
) => {
  const topOffset = input[0]
  const bottomOffset = input[1]
  //TODO: figure out if creating this for lots of elements slows down.
  //if so, useContext with ref that only creates it once
  const { scrollY } = useViewportScroll()
  // const [minHeight, setMinHeight] = useState('auto')

  const mv = useSpring(useTransform(scrollY, input, output), springConfig)

  return mv
}

export default useScrollEffect
