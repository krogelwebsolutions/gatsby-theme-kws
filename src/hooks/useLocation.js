//https://github.com/reach/router/issues/203
//TODO:check periodcally for package native implemenation
// https://github.com/ReactTraining/react-router/issues/6885
import { useMemo } from 'react'
import { useSubscription } from 'use-subscription'
import { globalHistory } from '@reach/router'

const useLocation = () => {
  const subscription = useMemo(
    () => ({
      getCurrentValue: () => globalHistory.location,
      subscribe: callback => globalHistory.listen(callback),
    }),
    []
  )

  return useSubscription(subscription)
}
export default useLocation
