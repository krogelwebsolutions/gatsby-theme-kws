import React from 'react'
// import { graphql, useStaticQuery } from 'gatsby'
/** @jsx jsx */
import { jsx } from 'theme-ui'
import styled from '@emotion/styled'
import { border, space, layout, color, position } from 'styled-system'
import Img from 'gatsby-image'
import { Box, Flex } from 'lib'
// import { useHover } from 'react-use-gesture'
// import { useSpring } from 'framer-motion'

//import

const Image = styled(Img)(border, space, layout, color, position)
// const ImageBase = styled(Img)(border, space, layout, color, position)

// const ImageWrapper = props => (
//   <Flex
//     // as={motion.div}
//     initial={{ scale: 0.96 }}
//     whileHover={{ scale: 1 }}
//     transition={{ duration: 1.6 }}
//     transformTemplate={({ scale }) => `scale(${scale})`}
//     {...props}
//   />
// )
//TODO;add optional support for grayscale filter (maybe all filters?)
const ImageWrapper = ({
  initialScale = 0.96,
  hoverScale = 1,
  transformTime = 3000,
  ...props
}) => {
  return (
    <Flex
      {...props}
      sx={{
        overflow: 'hidden',
        img: {
          transform: `scale(${initialScale})`,
          transition: `transform ${transformTime}ms, filter 3s ease-in-out, opacity 500ms ease 0s !important`,

          ':hover': {
            transform: `scale(${hoverScale})`,
          },
        },
      }}
    />
  ) //TODO:maybe pass _hover and _initial instead for more custimizavbility
}
Image.Wrapper = ImageWrapper
// ImageBase.Wrapper = ImageWrapper
// export default ImageBase
export default Image

// const Image = ({ src, ...props }) => {
//   const data = useStaticQuery(graphql`
//     query {
//       allFile(filter: { internal: { mediaType: { regex: "images/" } } }) {
//         edges {
//           node {
//             relativePath
//             childImageSharp {
//               fluid {
//                 ...GatsbyImageSharpFluid
//               }
//             }
//           }
//         }
//       }
//     }
//   `)

//   const match = useMemo(
//     () => data.allFile.edges.find(({ node }) => src === node.relativePath),
//     [data, src]
//   )

//   return <Img fluid={match.node.childImageSharp.fluid} {...props} />
// }

// export default Image
