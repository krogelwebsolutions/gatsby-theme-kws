import React from 'react'
import { List, ListItem, ListIcon } from '@chakra-ui/core'

ListItem.displayName = 'List.Item'
List.Item = ListItem

ListIcon.displayName = 'List.Icon'
List.Icon = ListIcon

export default List
