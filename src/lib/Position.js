import styled from '@emotion/styled'
import { Box } from './'

export const Relative = Box

Relative.defaultProps = {
  position: 'relative',
}

export const Absolute = Box

Absolute.defaultProps = {
  position: 'absolute',
}

export const Fixed = Box

Fixed.defaultProps = {
  position: 'fixed',
}

export const Sticky = Box

Sticky.defaultProps = {
  position: 'sticky',
}
