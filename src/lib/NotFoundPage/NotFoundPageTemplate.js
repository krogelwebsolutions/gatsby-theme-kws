import React from 'react'
import { Flex, Text, Image, Card } from 'lib'

// import { NotFoundImage } from './'

export const NotFoundPageTemplate = ({ heading, text, image }) => {
  return (
    <Flex bg="primary.50" grow center>
      <Card shrink boxShadow="small" center>
        <Image
          fluid={image.fluid}
          width={[200, null, 300, 400]}
          height={[200, null, 300, 400]}
          borderRadius="50%"
        />
        {/* <NotFoundImage
            width={[200, null, 300, 400]}
            height={[200, null, 300, 400]}
            borderRadius="50%"
          /> */}
        <Flex col center mx={[0, null, 4]} minWidth={300}>
          <Text.Heading as="h1" center>
            404
          </Text.Heading>
          <Text.Heading as="span" size="lg" center>
            {heading}
          </Text.Heading>
          <Text center>{text}</Text>
        </Flex>
      </Card>
    </Flex>
  )
}

export default NotFoundPageTemplate
