import React from 'react'
/** @jsx jsx */
import { jsx, useThemeUI } from 'theme-ui'
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  // ModalCloseButton,
} from '@chakra-ui/core'

// import { CloseButton } from 'lib'
import CloseButton from 'lib/CloseButton' //TODO:why is normal import not working

export const styles = {
  'close-btn': { position: 'absolute', top: '8px', right: '12px' },
}

ModalOverlay.displayName = 'Modal.Overlay'
Modal.Overlay = ModalOverlay

ModalContent.displayName = 'Modal.Content'
Modal.Content = ModalContent

ModalHeader.displayName = 'Modal.Header'
Modal.Header = ModalHeader

ModalFooter.displayName = 'Modal.Footer'
Modal.Footer = ModalFooter

ModalBody.displayName = 'Modal.Body'
Modal.Body = ModalBody

const ModalCloseButton = props => (
  <CloseButton sx={{ variant: 'modal.close-btn' }} {...props} />
)
CloseButton.displayName = 'Modal.CloseButton'
Modal.CloseButton = ModalCloseButton

// const EnhancedModal = ({ children, ...props }) => (
//   <Modal {...props}>{children}</Modal>
// )
export default Modal
