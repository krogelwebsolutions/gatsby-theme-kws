import React from 'react'
/** @jsx jsx */
import { jsx } from 'theme-ui'

import { Box, Flex, Image } from 'lib'
import { useTransition, a } from 'react-spring'
import * as ReactIs from 'react-is'

import { useResizeObserver, useResponsiveValue } from 'hooks'

const Masonry = ({ children, columns, ...props }) => {
  // Hook1: Tie media queries to the number of columns
  //TODO:check if columns is array or number. if number, dont do useResponsiveValue
  const columnCount = useResponsiveValue(columns)
  // Hook2: Measure the width of the container element
  // const [bind, { width }] = useResizeObserver()
  //TODO:consider returning object to destructure for readability
  const [ref, width, height] = useResizeObserver()
  // console.log(columnCount, 'columnCount')
  // Form a grid of stacked items using width & columns we got from hooks 1 & 2
  let heights = new Array(columnCount).fill(0) // Each column gets a height starting with zero

  //not working. React.Fragment.type maybe?
  const resolvedChildren = ReactIs.isFragment(children)
    ? children.props.children
    : children

  let gridItems = React.Children.map(resolvedChildren, (child, i) => {
    const childHeight = child.props.height //consider adding in support to resolve from sx prop
    const aspectRatio = child.props.aspectRatio //TODO: masonry has no idea what an aspect ratio is
    const columnWidth = width / columnCount

    const resolvedHeight = aspectRatio ? columnWidth / aspectRatio : childHeight

    const column = heights.indexOf(Math.min(...heights)) // Basic masonry-grid placing, puts tile into the smallest column using Math.min
    const xy = [
      (width / columnCount) * column,
      (heights[column] += resolvedHeight) - resolvedHeight,
    ] // X = container width / number of columns * column index, Y = it's just the height of the current column
    return {
      //...child.props,
      key: child.key, //change to id?
      component: child,
      xy,
      width: width / columnCount,
    }
  })
  // Hook5: Turn the static grid values into animated transitions, any addition, removal or change will be animated
  const transitions = useTransition(gridItems, gridItem => gridItem.key, {
    from: ({ xy }) => ({ xy, opacity: 0 }), //TODO:consider using scale to
    enter: ({ xy }) => ({ xy, opacity: 1 }), //reimplement removed xy/y anim
    update: ({ xy }) => ({ xy }),
    leave: { opacity: 0 },
    config: { mass: 5, tension: 500, friction: 100 },
    trail: 25,
  })

  // Render the grid
  return (
    <Box
      ref={ref}
      width="100%"
      height="100%"
      position="relative"
      style={{ height: Math.max(...heights) }}
      overflow="hidden"
    >
      {transitions.map(({ item, props: { xy, ...rest }, key }) => (
        <a.div
          sx={{
            width: item.width,
            position: 'absolute',
            willChange: 'transform, opacity',
            p: 3,
          }}
          key={key}
          style={{
            transform: xy.interpolate((x, y) => `translate3d(${x}px,${y}px,0)`),
            ...rest,
          }}
        >
          {item.component}
        </a.div>
      ))}
    </Box>
  )
}

Masonry.defaultProps = {
  columns: [2, 3, 3, 4, 6],
}
//TODO:enforce prop of masonry children. NEEDS key AND height

const MasonryItem = ({ key, height, ...rest }) => (
  <Flex key={key} height_={height} {...rest} />
)

MasonryItem.displayName = 'Masonry.Item'
Masonry.Item = MasonryItem

export default Masonry
//TODO: consider moving to /Layouts

//TODO:slowwww. try changing to framer from spring
