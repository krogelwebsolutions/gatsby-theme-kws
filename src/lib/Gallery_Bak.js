import React, { useState } from 'react'
/** @jsx jsx */
import { jsx } from 'theme-ui'
import Carousel, { Modal, ModalGateway } from 'react-images'
import { Box, Flex, Image } from 'lib'
import { Grid } from '@chakra-ui/core'

// import carouselFormatters from '../utils/carouselFormatters'

const chunk = (array, groupSize) => {
  const groups = []
  for (let i = 0; i < array.length; i += groupSize) {
    groups.push(array.slice(i, i + groupSize))
  }
  return groups
}

const sum = array =>
  array.reduce((accumulator, currentValue) => accumulator + currentValue)

//////

const Masonry = ({ children, gap = '1em', minWidth = 300 }) => {
  const cols = []
  const ref = React.useRef()
  const [numCols, setNumCols] = useState(2)

  const calcNumCols = () =>
    setNumCols(Math.floor(ref.current.offsetWidth / minWidth))

  const createCols = () => {
    for (let i = 0; i < numCols; i++) cols[i] = []
    children.forEach((child, i) => cols[i % numCols].push(child))
  }

  React.useEffect(() => {
    calcNumCols()
    window.addEventListener(`resize`, calcNumCols)
    return () => window.removeEventListener(`resize`, calcNumCols)
  })
  createCols()

  return (
    <Grid autoFlow="column" ref={ref} gap={gap}>
      {Array(numCols)
        .fill()
        .map((el, i) => (
          <Grid key={i} gap={gap} autoRows="max-content">
            {cols[i]}
          </Grid>
        ))}
    </Grid>
  )
}

const Gallery = ({
  images,
  itemsPerRow: itemsPerRowByBreakpoints = [2, 3, 4, 5, 8],
}) => {
  return (
    <Masonry>
      {images.map((image, i) => (
        <Image
          borderRadius=".5rem"
          fluid={image}
          // title={image.caption}
          height="100%"
          width="auto"
        />
      ))}
    </Masonry>
  )
}

const Gallery_ = ({
  images,
  itemsPerRow: itemsPerRowByBreakpoints = [2, 3, 4, 5, 8],
}) => {
  const aspectRatios = images.map(image => image.aspectRatio)
  const rowAspectRatioSumsByBreakpoints = itemsPerRowByBreakpoints.map(
    itemsPerRow =>
      chunk(aspectRatios, itemsPerRow).map(rowAspectRatios =>
        sum(rowAspectRatios)
      )
  )

  const [modalIsOpen, setModalIsOpen] = useState(false)
  const [modalCurrentIndex, setModalCurrentIndex] = useState(0)

  const closeModal = () => setModalIsOpen(false)
  const openModal = imageIndex => {
    setModalCurrentIndex(imageIndex)
    setModalIsOpen(true)
  }

  return (
    <Box>
      {images.map((image, i) => (
        <a
          key={image.id}
          href={image.originalImg}
          onClick={e => {
            e.preventDefault()
            openModal(i)
          }}
        >
          <Box
            sx={{
              display: 'inline-block',
              verticalAlign: 'middle',
              transition: '',
              ':hover': { filter: 'brightness(87.5%)' },

              //   borderWidth: '.5rem',
              //   borderColor: 'white',
              //   borderRadius: '.5rem',
              //   outlineOffset: '-.5rem',

              position: 'relative',
              '::after_': {
                content: '" "',
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: '100%',
                // bg: 'white',
                borderWidth: '.1rem',
                borderColor: 'white',
                outlineOffset: '1rem',
              },
            }}
            width={rowAspectRatioSumsByBreakpoints.map(
              (rowAspectRatioSums, j) => {
                const rowIndex = Math.floor(i / itemsPerRowByBreakpoints[j])
                const rowAspectRatioSum = rowAspectRatioSums[rowIndex]

                return `${(image.aspectRatio / rowAspectRatioSum) * 100}%`
                // return `calc(${(image.aspectRatio / rowAspectRatioSum) * 100}% - 1rem)`
              }
            )}
          >
            <Image
              borderRadius=".5rem"
              fluid={image}
              // title={image.caption}
              height="100%"
              width="auto"
              sx={{
                m: '.3rem',
                ':after_': {
                  content: '" ""',
                  position: 'absolute',
                  width: '100%',
                  height: '100%',
                  bg: 'red',
                },
              }}
            />
          </Box>
        </a>
      ))}

      {ModalGateway && (
        <ModalGateway>
          {modalIsOpen && (
            <Modal onClose={closeModal}>
              <Carousel
                views={images.map(({ originalImg, caption }) => ({
                  source: originalImg,
                  caption,
                }))}
                currentIndex={modalCurrentIndex}
                // formatters={carouselFormatters}
                components={{ FooterCount: () => null }}
              />
            </Modal>
          )}
        </ModalGateway>
      )}
    </Box>
  )
}

export default Gallery
