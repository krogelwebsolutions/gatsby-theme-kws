//https://github.com/chakra-ui/chakra-ui/blob/master/packages/chakra-ui/src/Icon/index.js
/** @jsx jsx */
import { jsx } from 'theme-ui'
import { ThemeContext } from '@emotion/core'
import { useContext, forwardRef } from 'react'
import { FaQuestionCircle } from 'react-icons/fa'
import Box from 'lib/Box'

export const styles = {
  default: { fontSize: 'inherit' },
  xs: {
    //  fontSize: ['2xs', 'xs', 'sm', 'md', 'lg'],
    fontSize: ['2xs', 'sm', 'md', 'lg'],
  },
  sm: {
    //  fontSize: ['xs', 'sm', 'md', 'lg', 'xl'],
    fontSize: ['xs', 'md', 'lg', 'xl'],
  },
  md: {
    //  fontSize: ['sm', 'md', 'lg', 'xl', '2xl'],
    fontSize: ['sm', 'lg', 'xl', '2xl'],
  },
  lg: {
    //  fontSize: ['md', 'lg', 'xl', '2xl', '3xl'],
    fontSize: ['md', 'xl', '2xl', '3xl'],
  },
  xl: {
    //  fontSize: ['lg', 'xl', '2xl', '3xl', '4xl'],
    fontSize: ['lg', '2xl', '3xl', '4xl'],
  },
  '2xl': {
    //  fontSize: ['xl', '2xl', '3xl', '4xl', '5xl'],
    fontSize: ['xl', '3xl', '4xl', '5xl'],
  },
  '3xl': {
    //  fontSize: ['2xl', '3xl', '4xl', '5xl', '6xl'],
    fontSize: ['2xl', '4xl', '5xl', '6xl'],
  },
  jumbo: {
    //  fontSize: ['4xl', '6xl', '7xl', '8xl', 'jumbo'],
    fontSize: ['4xl', '7xl', '8xl', 'jumbo'],
  },
}

const Icon = forwardRef(
  ({ size, name, color, role, focusable, ...rest }, ref) => {
    const { icons } = useContext(ThemeContext)

    const Component = icons[name] == null ? FaQuestionCircle : icons[name]

    return (
      <Box
        as={Component}
        ref={ref}
        size_={size}
        color={color}
        display="inline-block"
        verticalAlign="middle"
        focusable={focusable}
        role={role}
        sx={{
          flexShrink: 0,
          backfaceVisibility: 'hidden',
          variant: `icon.${size}`,
          //   '&:not(:root)': { overflow: 'hidden' },
        }}
        {...rest}
      />
    )
  }
)

Icon.defaultProps = {
  size: 'default',
  color: 'currentColor',
  role: 'presentation',
  focusable: false,
}

Icon.displayName = 'Icon'

export default Icon
