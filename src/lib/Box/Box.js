import React, { forwardRef, memo } from 'react'
/** @jsx jsx */
import { jsx } from 'theme-ui'
import styled from '@emotion/styled'
import {
  createShouldForwardProp,
  props,
} from '@styled-system/should-forward-prop'
import {
  background,
  border,
  color,
  flexbox,
  grid,
  layout,
  position,
  shadow,
  space,
  typography,
  compose,
} from 'styled-system'
import extraConfig from './config'
import { motion } from 'framer-motion'
import { useLayoutTransition } from 'hooks' //TODO:name useRouteTranstion

export const truncate = props => {
  if (props.isTruncated) {
    return {
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
    }
  }
}

export const systemProps = compose(
  layout,
  color,
  space,
  background,
  border,
  grid,
  position,
  shadow,
  typography,
  flexbox,
  extraConfig
)

const shouldForwardProp = createShouldForwardProp([
  ...props,
  'd',
  'textDecoration',
  'pointerEvents',
  'visibility',
  'transform',
  'cursor',
  'fill',
  'stroke',
])

/**
 * htmlWidth and htmlHeight is used in the <Image />
 * component to support the native `width` and `height` attributes
 *
 * https://github.com/chakra-ui/chakra-ui/issues/149
 */
const nativeHTMLPropAlias = ['htmlWidth', 'htmlHeight']
const framerMotionProps = ['whileHover', 'whileTap', 'animate', 'initial']

const allowProps = nativeHTMLPropAlias.concat(framerMotionProps)
/**
 * KWS
 * props to forward to framer-motion
 */

// const FramerBox = React.forwardRef(({ as = 'div', ...props }, ref) => {
//   const Component = motion.custom(as)
//   return <Component ref={ref} {...props} />
// })
// const Box = styled(motion.div, {
//   shouldForwardProp: prop => {
//     if (allowProps.includes(prop)) {
//       return true
//     } else {
//       return shouldForwardProp(prop)
//     }
//   },
// })(truncate, systemProps)

// const framerProps = ['animate, variants']

// const Box = styled(FramerBox)(truncate, systemProps)
// const Box = styled(motion.div)(truncate, systemProps)
const BaseBox = styled('div', {
  // shouldForwardProp: prop => {
  //   if (allowProps.includes(prop)) {
  //     return true
  //   } else {
  //     return shouldForwardProp(prop)
  //   }
  // },
})(truncate, systemProps)
BaseBox.displayName = 'BaseBox'
/**
 * _landscape is a orientation selector
 */
const Box = forwardRef(({ _landscape, _portrait, ...props }, ref) => {
  //TODO:at first guess, manually passing this to box should not be necessary.
  //create code sandbox that demonstrates that using "string" animate keys that it passes down, but using controls does not pass down
  // const { controls } = useLayoutTransition //think this is causing re-renders
  // const [{ controls }] = useLayoutTransition() //think this is causing re-renders

  //options to try:
  //1) removing styled system in favor for sx prop only (to make usememo easier)
  //2) adding more functions to the controls object (to track state and not cause any rerenders)

  if (props.animate || props.variants || props.transition || props.initial) {
    // let animate = null
    // if (props.variants && (props.variants.enter || props.variants.exit)) {
    //   animate = controls
    // }
    // const animate =
    //   props?.variants?.enter || props?.variants?.exit ? controls : null
    return (
      <BaseBox
        //props should be passed at bottom so we can override animate controls with one off animations
        sx={{
          '@media (orientation: landscape)': { ..._landscape },
          '@media (orientation: portrait)': { ..._portrait },
        }}
        // animate={animate}
        {...props}
        as={motion.div}
        ref={ref}
      />
    )
  } else {
    return (
      <BaseBox
        {...props}
        sx={{
          '@media (orientation: landscape)': { ..._landscape },
          '@media (orientation: portrait)': { ..._portrait },
        }}
        ref={ref}
      />
    )
  }
})
Box.displayName = 'Box'
// Box.whyDidYouRender = true

export default Box

//TODO: styled(motion.div) breaks places wr are still using styled(animated.div)
//try using styled('div') and  using as={motion.div} without using shouldForwardProp
//or change out places we are using as={animated.div} to framer-motion

//TODO: consider adding back in functionality from
// import { PseudoBox as Box } from '@chakra-ui/core'
