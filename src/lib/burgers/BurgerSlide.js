import React from 'react'
import Burger from '@animated-burgers/burger-slide'
import '@animated-burgers/burger-slide/dist/styles.css'

const BurgerSlide = props => <Burger {...props} />
export default BurgerSlide
