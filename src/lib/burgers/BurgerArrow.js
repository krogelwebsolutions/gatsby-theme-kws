import React from 'react'
import Burger from '@animated-burgers/burger-arrow'
import '@animated-burgers/burger-arrow/dist/styles.css'

const BurgerArrow = props => <Burger {...props} />
export default BurgerArrow
