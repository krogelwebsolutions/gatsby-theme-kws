import React from 'react'
import Burger from '@animated-burgers/burger-rotate'
import '@animated-burgers/burger-rotate/dist/styles.css'

const BurgerRotate = props => <Burger {...props} />
export default BurgerRotate
