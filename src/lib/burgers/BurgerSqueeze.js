import React from 'react'
import Burger from '@animated-burgers/burger-squeeze'
import '@animated-burgers/burger-squeeze/dist/styles.css'

const BurgerSqueeze = props => <Burger {...props} />
export default BurgerSqueeze
