import { Flex } from './'
import styled from '@emotion/styled'
import PropTypes from 'prop-types'
import { layout, space } from 'styled-system'
import propTypes from '@styled-system/prop-types'
import themeGet from '@styled-system/theme-get' //TODO:we may not need this utility after a cleanup of the code

const gutter = props => {
  const gutterValue = themeGet(`gutters.${props.gutter}`, props.gutter)(props)

  const gutterStyles = unit => ({
    paddingLeft: themeGet(`space.${unit}`)(props),
    paddingRight: themeGet(`space.${unit}`)(props),
  })

  if (!gutterValue) {
    return null
  }

  if (!Array.isArray(gutterValue)) {
    return gutterStyles(gutterValue)
  }

  return gutterValue.map((value, index) => {
    if (index === 0) {
      return gutterStyles(value)
    }

    return {
      [themeGet(`mediaQueries.${index - 1}`)(props)]: gutterStyles(value),
    }
  })
}

const Container = styled(Flex)`
  ${layout}
  ${space}
`

Container.propTypes = {
  gutter: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.arrayOf(
      PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    ),
  ]), //TODO:do we still need this?
  ...propTypes.layout,
  ...propTypes.space,
}

Container.defaultProps = {
  width: '100%',
  maxWidth: 'xl',
  marginLeft: 'auto',
  marginRight: 'auto',
  vAlignContent: 'center',
  hAlignContent: 'center',
  gutter: 'default',
}
const ContainerExtraSmall = styled(Container)``
ContainerExtraSmall.defaultProps = { maxWidth: 'xs' }

const ContainerSmall = styled(Container)``
ContainerSmall.defaultProps = { maxWidth: 'sm' }

const ContainerMedium = styled(Container)``
ContainerMedium.defaultProps = { maxWidth: 'md' }

const ContainerLarge = styled(Container)``
ContainerLarge.defaultProps = { maxWidth: 'lg' }

Container.displayName = 'Container'

Container.xs = ContainerExtraSmall
Container.sm = ContainerSmall
Container.md = ContainerMedium
Container.lg = ContainerLarge
//TODO: think about Container.xs vs Container size="xs"
export default Container
