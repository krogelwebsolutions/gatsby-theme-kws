import styled from '@emotion/styled'
import { space } from 'styled-system'

const Spacer = styled.div(space)

Spacer.defaultProps = { m: 'auto' }

export default Spacer
