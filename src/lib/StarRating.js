//https://github.com/hooroo/roo-ui/blob/alpha/src/components/StarRating/StarRating.js
import React from 'react'
import PropTypes from 'prop-types'
import { Flex } from 'lib'
import range from 'lodash/range'
import { FaStar, FaStarHalfAlt } from 'react-icons/fa'

const StarIcon = ({ rating, index, ...rest }) => {
  if (index < rating - 0.5) {
    return <FaStar {...rest} />
  } else {
    return <FaStarHalfAlt {...rest} />
  }
}

const renderRating = ({ rating, size }) => {
  const ratingItems = []
  //used to be lodash/range(5)
  range(5).forEach(index => {
    ratingItems.push(<StarIcon rating={rating} size={size} index={index} />)
  })

  return ratingItems
}

const StarRating = ({ ratingType, rating, size }) => (
  <Flex
    row
    itemType="http://schema.org/AggregateRating"
    aria-label={`${rating} out of 5 rating`}
    title={`${rating} out of 5 rating`}
    color="yellow"
  >
    {renderRating({ ratingType, rating, size })}
  </Flex>
)

const stringOrNumber = PropTypes.oneOfType([PropTypes.string, PropTypes.number])

StarRating.propTypes = {
  rating: stringOrNumber.isRequired,
  ratingType: PropTypes.oneOf(['AAA', 'SELF_RATED']).isRequired,
  size: stringOrNumber.isRequired,
}

export default StarRating
