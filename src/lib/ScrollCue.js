import React from 'react'
//TODO:remove emotion styled from dependencies if we fully factor it out
/** @jsx jsx */
import { jsx, useThemeUI } from 'theme-ui'
import { Flex, Icon } from './'
import { motion } from 'framer-motion'

///TODO:change all animated.div to animated(Box)
export const styles = {
  borderRadius: 'default',
  // borderWidth: '1px',
  boxShadow: 'sm',
}

//TODO: use timing prop in transition to have better control over transition
//ie, have a finalDelay prop for the when all are invisible, and another prop for how fast they arrows move
const AnimatedArrow = ({ index, duration, ...props }) => (
  <motion.div
    animate_="scroll"
    variants={{
      scroll: {
        opacity: [0, 1, 0, 0],
        y: ['-30px', '-15px', '0px', '0px'],
        transition: { loop: Infinity, duration },
      },
    }}
    custom={index}
    sx={
      {
        // position: 'absolute',
        // top: `-${index * 50}%`,
        // height: '3rem',
        // width: '3rem',
        // mt: `-${index * 0.5}rem`,
        // mt: `${-index * 10}%`,
      }
    }
    {...props}
  >
    <Icon name="scroll-cue" {...props} />
  </motion.div>
)
const ScrollCue = ({
  size,
  number = 3,
  staggerChildren = 0.3,
  duration = 2,
  ...props
}) => {
  const arrows = []
  for (let i = 0; i < number; i++) {
    arrows.push(<AnimatedArrow size={size} index={i} duration={duration} />)
  }

  return (
    <Flex
      as={motion.div}
      animate="scroll"
      variants={{
        scroll: {
          transition: { staggerChildren },
        },
      }}
      hAlignContent="center"
      position="relative"
      {...props}
    >
      {arrows}
    </Flex>
  )
}

ScrollCue.defaultProps = { size: 'lg' }

export default ScrollCue
