import React from 'react'
import { Flex, Box, Grid, Text } from 'lib'
import { Buttons } from './'

const DesignSystemPreview = props => (
  <Flex col pt={['3rem', null, '4rem']}>
    <Buttons />
  </Flex>
)

export default DesignSystemPreview
