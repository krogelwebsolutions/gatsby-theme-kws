import React from 'react'
import { Flex, Box, Grid, Text, Button } from 'lib'
//TODO:just iterate over key sizes in variant definition instead

//TODO:spacing does not work with landscape media queries (when row orientation locked, spacing is stil on botton)
const Buttons = props => (
  <Grid gap={2} rows={1} minChildWidth={'5em'} orientationLocked>
    <Button size="xs" variantColor="primary">
      xs
    </Button>
    <Button size="sm" variantColor="primary">
      sm
    </Button>
    <Button size="md" variantColor="primary">
      md
    </Button>
    <Button size="lg" variantColor="primary">
      lg
    </Button>
    <Button size="xl" variantColor="primary">
      xl
    </Button>
  </Grid>
)

export default Buttons
