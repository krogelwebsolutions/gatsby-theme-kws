import React, { useState } from 'react'
/** @jsx jsx */
import { jsx, useThemeUI } from 'theme-ui'
import Carousel, { Modal, ModalGateway } from 'react-images'
import FsLightbox from 'fslightbox-react'
import ReactGallery from 'react-photo-gallery'
import { motion } from 'framer-motion'
import { Box, Flex, Image, Masonry, Text, Icon } from 'lib' //Modal
import { useToggle } from 'hooks'
import { Global } from '@emotion/core'

export const styles = {
  columns: [2, 4, 4, 5, 6],
  thumbnail: {
    borderRadius: 'default',
    ':hover': { filter: 'brightness(87.5%)' },
  },
}

const transitionVariants = {
  //TODO:create lib of these (FadeSlideIn)
  enter: {
    y: 0,
    opacity: 1,
    transition: {
      y: { stiffness: 1000, velocity: -100 },
    },
  },
  exit: {
    y: 50,
    opacity: 0,
    transition: {
      y: { stiffness: 1000 },
    },
  },
}

const Gallery = ({ images, thumbs, ...props }) => {
  const { theme } = useThemeUI()

  const [isOpen, toggle] = useToggle()
  //TODO:finish when chakra fixes scrollbar flicker
  //https://github.com/chakra-ui/chakra-ui/issues/129
  const [modalCurrentIndex, setModalCurrentIndex] = useState(0)

  const openModal = imageIndex => {
    setModalCurrentIndex(imageIndex)
    toggle()
  }

  //TODO:work in Image.Wrapper for nice hover effect
  const imageRenderer = React.useCallback(
    ({ index, left, top, key, photo }) => (
      <motion.a
        key={photo.id}
        href={photo.originalImg}
        sx={{
          m: ['3px', '1rem'],
          left,
          top,
          width: photo.width,
          height: photo.height,
          position: 'absolute',
        }}
        onClick={e => {
          e.preventDefault()
          // toggle()
          openModal(index)
        }}
        variants={transitionVariants}
      >
        <Image
          sx={{ variant: 'gallery.thumbnail' }}
          fluid={photo.fluid}
          // title={image.caption}
          // width="100%"
          // height="100%"
          height={photo.height}
          width={photo.width}
          // left={left}
          // top={top}
        />
        <motion.div
          initial={{ opacity: 0 }}
          whileHover={{ opacity: 1 }}
          sx={{
            bg: 'rgba(0,0,0,.5)',
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            cursor: 'pointer',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: theme.gallery.thumbnail.borderRadius,
          }}
        >
          <Icon name="zoom-in" size="xl" color="white" />
        </motion.div>
      </motion.a>
    ),
    []
  )

  return (
    <motion.div
      variants={{
        enter: {
          transition: { staggerChildren: 0.07, delayChildren: 0.2 },
        },
        exit: {
          transition: { staggerChildren: 0.05, staggerDirection: -1 },
        },
      }}
    >
      <ReactGallery photos={thumbs} renderImage={imageRenderer} {...props} />

      <FsLightbox
        toggler={isOpen}
        customSources={images.map(({ fluid, caption, presentationWidth }) => {
          return (
            <Flex
              width="90vw"
              height="90vh"
              vAlignContent="center"
              hAlignContent="center"
            >
              <Image
                objectFit="contain"
                height="100%"
                width="100%"
                fluid={fluid}
                style={{ maxWidth: presentationWidth }}
                alt={caption}
                imgStyle={{ objectFit: 'contain' }}
              />
              {caption && (
                <Text
                  position="absolute"
                  bottom={0}
                  color="white"
                  sx={{ transform: 'translateY(100%)' }}
                >
                  {caption}
                </Text>
              )}
            </Flex>
          )
        })}
        sourceIndex={modalCurrentIndex}
      />
      <Global styles={{ '.react-images__view-image': { display: 'inline' } }} />
    </motion.div>
  )
}

const View = props => {
  // console.log(props, '*')
  return (
    <Flex>
      <span>Test</span>
      <Image height="100%" width="auto" fluid={props.data} />
    </Flex>
  )
}

//NOTE: For some reason, a few images were displaying with weird gaps/padding
//due to the aspect ratio reported from image-sharp query being incorrect.
//I have no idea why.  The fix is to open the file in windows photo editor
//and resave/overwrite
export default Gallery
