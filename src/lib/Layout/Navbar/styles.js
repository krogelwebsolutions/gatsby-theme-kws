import { styles as navlink } from './NavLink'
export default {
  logo: { p: 2, ml: 4 },
  container: {
    backgroundColor: 'rgba(255,255,255,0.80)',
    // backgroundImage:
    //   'linear-gradient(rgb(255, 255, 255), rgba(255, 255, 255, 0.25))',
    borderBottom: '1px',
    borderColor: 'gray.100',
    backdropFilter: 'blur(6px)',
    color: 'primary.500',
    letterSpacing: 'wide',
    textTransform: 'uppercase',
    fontFamily: 'heading',
    fontWeight: 'medium',
    // px: 4,
    //py: [0, 0, 0, 2, 3],
  },
  navlink,
}
