import React from 'react'
/** @jsx jsx */
import { jsx } from 'theme-ui'
// import { Link } from 'gatsby'
import { Flex } from 'lib'
import TransitionLink from 'lib/Layout/TransitionContext/TransitionLink' //TODO:clean up
import { useLocation } from 'hooks'

export const styles = {
  container: {
    backgroundColor: 'none',
    // color: '',
    transition: '500ms ease-in-out',
    ':hover': { backgroundColor: 'primary.500' },
    '.active': {
      // fontWeight: 'bold',
      color: 'primary.500',
      // borderBottom: '2px',
    },
    //transition: 'slow',//transition theme key not working
    // my: 3,

    // mr: 3,
    // px: 2,
    position: 'relative',
    height: '100%',
  },
  inner: {
    lineHeight: 'taller',
    // lineHeight: '100px',
    // py: 2,
    display: 'flex',
    height: '100%',
    alignItems: 'center',
    px: 4,
  },
}

const isCurrent = (pathname, currentPathname) => {
  // const retVal = pathname === currentPathname
  if (pathname === '/') {
    return currentPathname === pathname
  } else {
    return currentPathname.startsWith(pathname)
  }
}

//TODO: animate (probably move to menu level and move it to active like a cursor)
const CurrentLinkFx = props => (
  <Flex
    sx={{
      height: '2px',
      width: '100%',
      position: 'absolute',
      bottom: '1px',
      backgroundColor: 'primary.500',
    }}
  />
)

const NavLink = props => {
  const { pathname } = useLocation()
  const add = isCurrent(props.to, pathname)
  // console.log(location, 'loc')
  // const borderBottom = underline && '2px'
  return (
    <Flex
      as="li"
      vAlignContent="center"
      justifyContent="center"
      sx={{
        variant: 'layout.navbar.navlink.container',
        // borderBottom,
      }}
    >
      <TransitionLink
        activeClassName="active"
        {...props}
        sx={{
          variant: 'layout.navbar.navlink.inner',
        }}
      />
      {isCurrent(props.to, pathname) && <CurrentLinkFx />}
    </Flex>
  )
}

//TODO:change to ul/li (also change margin from mr to :last selector)
// const NavLink = props => {
//   return (
//     <Flex
//       as="li"
//       vAlignContent="center"
//       justifyContent="center"
//       sx={{
//         variant: 'layout.navbar.navlink.container',
//       }}
//     >
//       <Link
//         activeClassName="active"
//         {...props}
//         sx={{
//           variant: 'layout.navbar.navlink.inner',
//         }}
//       />
//     </Flex>
//   )
// }

export default NavLink
