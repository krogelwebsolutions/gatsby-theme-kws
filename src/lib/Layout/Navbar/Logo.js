import React from 'react'
import { Image } from 'lib'
import { graphql, useStaticQuery } from 'gatsby'

const Logo = props => {
  const data = useStaticQuery(graphql`
    query NavbarLogoQuery {
      logo: file(
        sourceInstanceName: { eq: "theme-assets" }
        name: { eq: "krogel" }
      ) {
        childImageSharp {
          fixed(height: 82) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `)
  return <Image fixed={data.logo.childImageSharp.fixed} {...props} />
}

export default Logo
