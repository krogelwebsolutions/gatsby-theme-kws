import React, { useEffect } from 'react'
// import { useLayoutTransition, useLocation } from 'hooks' //TODO:name useRouteTranstion
import { useTransitionActions } from 'lib/Layout/TransitionContext/TransitionContext'

// import React from 'react'

// exports.wrapPageElement = require(`./wrap-page`)

const PageWrapper = ({ children, ...props }) => {
  // const [transitionState, dispatch] = useLayoutTransition()
  // const [transitionState, actions] = useLayoutTransition()
  const actions = useTransitionActions()
  // useEffect(actions.transitionIn, [])
  useEffect(() => {
    console.log('transitionstate-page-load')
    actions.triggerTransitionIn()
  }, [children])
  // console.log('transitionState', transitionState)

  // useEffect(() => dispatch({ type: 'animations-complete' }), [])
  // useEffect(() => {
  //   // dispatch({ type: 'animations-complete' })
  //   dispatch({ type: 'navigation-complete' })
  //   console.log('navigation-complete!')
  // }, [children])
  return <>{children}</>
}

const wrapPageElement = ({ element, props }) => (
  <PageWrapper {...props}>{element}</PageWrapper>
)

export default wrapPageElement
