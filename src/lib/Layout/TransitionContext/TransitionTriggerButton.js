import React, { useState } from 'react'
import { useLocation, useToggle } from 'hooks' //TODO:name useRouteTranstion
import { useTransitionActions } from 'lib/Layout/TransitionContext/TransitionContext'
import { Button } from 'lib'
/*
 * Button to trigger enter/exit layout transitions without navigating away from the current route
 */
const TransitionTriggerButton = props => {
  // const [transitionState, dispatch] = useLayoutTransition()
  const actions = useTransitionActions()
  const [hasEntered, toggle] = useToggle(false)
  const variant = hasEntered ? 'enter' : 'exit'
  // console.log('transitionState', transitionState)
  console.log('transitionState-actions', actions)
  const onClick = event => {
    console.log('trigger button clicked')
    event.persist()
    event.preventDefault()

    // if (transitionState.spring === 'enter') {
    // } else {
    //   transitionState.triggerEnter()
    // }

    // if (
    //   //TODO:have spereate state for variant  (enter/exit)
    //   transitionState.status === 'entering' ||
    //   transitionState.status === 'entered'
    // ) {
    //   actions.transitionOut()
    // } else {
    //   // transitionState.triggerEnter()
    //   actions.transitionIn()
    // }
    if (variant === 'enter') {
      actions.triggerTransitionOut()
    } else {
      actions.triggerTransitionIn()
    }
    toggle()
    // transitionState.spring === 'enter'
    //   ? dispatch({ type: 'animate-out' })
    //   : dispatch({ type: 'animate-in' })
    // dispatch({ type: 'animations-complete' })
    // transitionState.triggerExit()

    // transitionState.controls.start('exit')
    // console.log('transitionState-trigger', transitionState.controls)
  }
  return (
    <Button.Outline
      sx={{
        position: 'fixed',
        // top: '50%',
        // left: '50%',
        // bottom: '50%',
        // right: '50%',
        left: 0,
        bottom: 0,
        zIndex: 99999,
      }}
      color="primary.500"
      onClick={onClick}
      {...props}
    >
      Trigger Transition Step
    </Button.Outline>
  )
}

export default TransitionTriggerButton
