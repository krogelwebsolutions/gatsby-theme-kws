import React from 'react'
import { Link } from 'gatsby'
import { useLocation } from 'hooks'
import { useTransitionActions } from 'lib/Layout/TransitionContext/TransitionContext'
import { interceptRoute } from './interceptor'

// const AnimatedLink = props => (
//   <TransitionLink
//     exit={{ length: 0.4 }}
//     entry={{ length: 0.4, delay: 0.4 }}
//     {...props}
//   />
// )

//TODO:extract out a navigate function for progrmmatic use

// const currentPath = location.pathname
// export const navigate = async ({ to, state, replace }) => {
//   console.log('navigate() before intercept')

//   to = await interceptRoute(currentPath, to)
//   console.log(to, 'navigate() after interceptroute')

//   if (!to || to === currentPath) {
//     return
//   }

//   // dispatch({
//   //   type: 'navigate-to',
//   //   payload: { to, replace, linkState: state },
//   // })
//   console.log('*******navigated to new page****')
// }

const TransitionLink = ({
  to,
  state,
  replace,
  handleClick,
  delay,
  ...props
}) => {
  // const [transitionState, dispatch] = useLayoutTransition()
  const actions = useTransitionActions()
  const location = useLocation()
  const onClick = event => {
    event.persist()
    event.preventDefault()
    handleClick && handleClick() //TODO:we may not need to pass down toggle for nav from here. instead use framer motion variants

    const payload = { to, replace, linkState: state }
    if (location.pathname !== to) {
      console.log(location.pathname, to)
      // if (delay) {
      //   payload.delay = delay
      // }
      actions.navigate(payload)
      // // dispatch({ type: 'animate-out' })
      // transitionState.controls.start('exit')
      // // dispatch({ type: 'navigate-to' })
      // dispatch({
      //   type: 'navigate-to',
      //   payload,
      // })
    }
    //
    // navigate({ to, state, replace })
  }
  return <div onClick={onClick} to={to} {...props} />
}
export default TransitionLink
