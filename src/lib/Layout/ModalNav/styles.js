import { styles as navlink } from './NavLink'
import { styles as toggle } from './NavToggle'

export default {
  toggle,
  logo: { py: 8 },
  container: {
    backgroundColor: 'secondary.500',
    color: 'primary.500',
    boxShadow: 'inset 0 -0.5rem 1rem rgba(0, 0, 0, 0.07)',
  },
  menu: {
    color: 'primary.500',
    letterSpacing: 'wide',
    textTransform: 'uppercase',
    fontFamily: 'heading',
    fontWeight: 'medium',
  },
  navlink,
}
