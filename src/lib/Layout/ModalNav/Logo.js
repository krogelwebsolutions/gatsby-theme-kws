import React from 'react'
import { Image } from 'lib'
import { graphql, useStaticQuery } from 'gatsby'

const Logo = props => {
  const data = useStaticQuery(graphql`
    query ModalNavLogoQuery {
      logo: file(
        sourceInstanceName: { eq: "theme-assets" }
        name: { eq: "brand-vertical" }
      ) {
        childImageSharp {
          fixed(width: 220) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `)
  return (
    <Image
      fixed={data.logo.childImageSharp.fixed}
      alt="Krogel Web Solutions - Logo"
      {...props}
    />
  )
}

export default Logo
