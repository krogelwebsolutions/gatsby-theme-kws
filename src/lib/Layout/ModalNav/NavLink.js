import React from 'react'
/** @jsx jsx */
import { jsx } from 'theme-ui'
import PropTypes from 'prop-types'
// import { Link } from 'gatsby'
import { FaLongArrowAltRight } from 'react-icons/fa'
import { Flex, Hide } from 'lib'
import TransitionLink from 'lib/Layout/TransitionContext/TransitionLink' //TODO:clean up

export const styles = {
  container: {
    listStyle: 'none',
    padding: 1,

    '.active': {
      fontWeight: 'bold',
      color: 'primary.600',
    },
  },
  inner: {
    cursor: 'pointer',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    transform: 'translate3d(2rem, 0, 0)',
    // transition: `all ${transitions.quick}`,
    transition: `all 222ms cubic-bezier(0.060, 0.975, 0.195, 0.985)`,
    svg: {
      display: 'inline',
      fontSize: '2rem',
      opacity: 0,
      transform: 'translate3d(6rem, 0, 0)',
      transition: `all 222ms cubic-bezier(0.060, 0.975, 0.195, 0.985)`,
    },
    '&:hover': {
      // transform: 'translate3d(-3rem, 0, 0)',
      transition: `all 222ms cubic-bezier(0.060, 0.975, 0.195, 0.985)`,
      svg: {
        opacity: 1,
        transform: 'translate3d(1rem, 0, 0)',
        transition: `all 222ms cubic-bezier(0.060, 0.975, 0.195, 0.985)`,
      },
    },
  },
}

const Wrapper = props => (
  <Flex
    as="li"
    row
    vAlignContent="center"
    wrap="nowrap"
    sx={{
      variant: 'layout.modalNav.navlink.container',
    }}
    {...props}
  />
)

const CustomLink = ({ children, ...props }) => {
  return (
    <TransitionLink
      sx={{
        variant: 'layout.modalNav.navlink.inner',
      }}
      {...props}
    >
      {children}
      {/* <Hide touch important> */}
      <FaLongArrowAltRight />
      {/* </Hide> */}
    </TransitionLink>
  )
}

const NavLink = ({ children, to, toggle }) => {
  return (
    <Wrapper>
      <CustomLink onClick={toggle} to={to} activeClassName="active">
        {children}
      </CustomLink>
    </Wrapper>
  )
}

NavLink.propTypes = {
  text: PropTypes.string,
  to: PropTypes.string,
}

NavLink.defaultProps = {
  to: `/`,
}

export default NavLink
