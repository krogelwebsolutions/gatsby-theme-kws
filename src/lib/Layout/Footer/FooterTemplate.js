import React from 'react'
import { Text, GrecaptchaBranding } from 'lib'

const FooterTemplate = ({ title, date, showKWS, showRecaptcha, ...rest }) => (
  <>
    <Text textAlign="center" size="sm">
      © {title} {date}. All rights reserved.
    </Text>
    {showKWS && (
      <Text textAlign="center" size="sm">
        Design: Krogel Web Solutions
      </Text>
    )}
    {showRecaptcha && <GrecaptchaBranding />}
  </>
)

FooterTemplate.defaultProps = {
  showKWS: true,
  showRecaptcha: true,
  date: new Date().getFullYear(),
}

export default FooterTemplate
