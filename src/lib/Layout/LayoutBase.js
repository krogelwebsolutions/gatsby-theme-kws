import React from 'react'
/** @jsx jsx */
import { jsx } from 'theme-ui'
import { Helmet } from 'react-helmet'
import { Flex, ScrollProgress } from 'lib'
import { useSiteMetadata } from 'hooks'
import { CSSReset, useTheme } from '@chakra-ui/core'
import { Global } from '@emotion/core'

const DocHead = () => {
  const { title, description } = useSiteMetadata()
  //TODO:update icons in head
  return (
    <Helmet>
      <html lang="en" />
      <title>{title}</title>
      <meta name="description" content={description} />

      <link
        rel="apple-touch-icon"
        sizes="180x180"
        href="images/apple-touch-icon.png"
      />
      <link
        rel="icon"
        type="image/png"
        href="images/favicon-32x32.png"
        sizes="32x32"
      />
      <link
        rel="icon"
        type="image/png"
        href="images/favicon-16x16.png"
        sizes="16x16"
      />

      <link
        rel="mask-icon"
        href="/images/safari-pinned-tab.svg"
        color="#ff4400"
      />
      <meta name="theme-color" content="#fff" />
      <meta property="og:type" content="business.business" />
      <meta property="og:title" content={title} />
      <meta property="og:url" content="/" />
      <meta property="og:image" content="/images/og-image.jpg" />
    </Helmet>
  )
}

// const PageContainer = ({ children, ...rest }) => {
const PageContainer = React.forwardRef(({ children, ...rest }, ref) => {
  return (
    <Flex position="relative" height="100%" {...rest}>
      {children}
    </Flex>
  )
})
PageContainer.displayName = 'LayoutBase.PageContainer'
// const Header = props => <Flex {...props} as="header" shrink={false} />
const Header = rest => {
  return (
    <Flex
      as="header"
      shrink={false}
      {...rest}
      sx={{ variant: 'layout.header' }}
    />
  )
}
Header.displayName = 'LayoutBase.Header'

//TODO: look at rebass/components for defining these as a function
const Footer = props => (
  <Flex as="footer" {...props} sx={{ variant: 'layout.footer' }} />
)
Footer.displayName = 'Footer'

const Main = ({ children, ...props }) => {
  return (
    <Flex
      ref_="{scrollRef}"
      grow
      as="main"
      sx={{ variant: 'layout.main', flexGrow: 1 }}
      {...props}
    >
      {children}
    </Flex>
  )
}
Main.displayName = 'LayoutBase.Main'

//TODO:can probably get rid of pagecontainer by using emotion global
const LayoutBase = ({ children, isFullWidth = false, ...props }) => {
  const theme = useTheme()

  return (
    <>
      <DocHead />
      <CSSReset config={theme.config.chakraConfig} />

      <Global
        styles={theme => ({
          body: {
            MozOsxFontSmoothing: 'grayscale',
            WebkitFontSmoothing: 'antialiased',
            fontFamily: theme.fonts.body,
            //
            // position: 'fixed',
            // WebkitOverflowScrolling: 'touch',
            // overflowY: 'scroll',
            // overfow: 'hidden',
            height: '100%',
            width: '100%',

            //from animation:
            // overflow: visible ? `hidden` : `visible`,
          },
          '.grecaptcha-badge': { display: 'none' },
          html: {
            height: '100%',
            width: '100%',
            overscrollBehavior: 'none',
            // overflow: 'hidden',
          },
          '#___gatsby': { height: '100%' },
          '#gatsby-focus-wrapper': { height: '100%' },
          // '::-webkit-scrollbar': { width: 0 }, //add hidescrollbar config
          // scrollbarWidth: 'none',
        })}
      />
      <PageContainer isFullWidth={isFullWidth} {...props}>
        {children}
      </PageContainer>
    </>
  )
}
LayoutBase.displayName = 'LayoutBase'

LayoutBase.Header = Header
LayoutBase.Footer = Footer
LayoutBase.Main = Main

export default LayoutBase
