import React from 'react'
import { Text } from 'lib'
/** @jsx jsx */
import { jsx, useThemeUI } from 'theme-ui'
// import Truncate from '@konforti/react-truncate'

const Truncate = ({ length, children, ...rest }) => {
  let text = children
  if (children && children.length > length) {
    text = children.substring(0, length) + '...'
  }
  return <Text {...rest}>{text}</Text>
}

// const Truncate = ({ children, ...rest }) => {
//   return (
//     <Text
//       {...rest}
//       sx={{
//         overflow: 'hidden',
//         position: 'relative',
//         lineHeight: '1.2em',
//         maxHeight: '3.6em',
//         textAlign: 'justify',
//         mr: '-1em',
//         ml: '-1em',

//         ':before': {
//           content: '...',
//           position: 'absolute',
//           right: 0,
//           bottom: 0,
//         },
//         ':after': {
//           content: '""',
//           position: 'absolute',
//           right: 0,
//           width: '1em',
//           height: '1em',
//           marginTop: '0.2em',
//           background: 'white',
//         },
//       }}
//     >
//       {children}
//     </Text>
//   )
// }
export default Truncate
//TODO:make sure children is just a string
