import React from 'react'
/** @jsx jsx */
import { jsx } from 'theme-ui'
import { hideVisually } from 'polished'
import { Box } from 'lib'

//can merge this in with main text component below
const TextBase = React.forwardRef((props, ref) => {
  return <Box ref={ref} as="p" fontFamily="body" {...props} />
})

// import Truncate from '@konforti/react-truncate'

//TODO:remember to remove utils/omitprops when finished refactoring

// const textDecoration = style({
//   prop: 'textDecoration',
//   cssProperty: 'textDecoration',
// })

// const textTransform = style({
//   prop: 'textTransform',
// })
export const styles = {
  headings: { fontWeight: 'bold', fontFamily: 'heading' },
  sizes: {
    xs: {
      //  fontSize: ['2xs', 'xs', 'sm', 'md', 'lg'],
      fontSize: ['2xs', 'sm', 'md', 'lg'],
    },
    sm: {
      //  fontSize: ['xs', 'sm', 'md', 'lg', 'xl'],
      fontSize: ['xs', 'md', 'lg', 'xl'],
    },
    md: {
      //  fontSize: ['sm', 'md', 'lg', 'xl', '2xl'],
      fontSize: ['sm', 'lg', 'xl', '2xl'],
    },
    lg: {
      //  fontSize: ['md', 'lg', 'xl', '2xl', '3xl'],
      fontSize: ['md', 'xl', '2xl', '3xl'],
    },
    xl: {
      //  fontSize: ['lg', 'xl', '2xl', '3xl', '4xl'],
      fontSize: ['lg', '2xl', '3xl', '4xl'],
    },
    '2xl': {
      //  fontSize: ['xl', '2xl', '3xl', '4xl', '5xl'],
      fontSize: ['xl', '3xl', '4xl', '5xl'],
    },
    '3xl': {
      //  fontSize: ['2xl', '3xl', '4xl', '5xl', '6xl'],
      fontSize: ['2xl', '4xl', '5xl', '6xl'],
    },
    jumbo: {
      //  fontSize: ['4xl', '6xl', '7xl', '8xl', 'jumbo'],
      fontSize: ['5xl', '8xl', '9xl', 'jumbo'],
    },
  },
}

//TODO: figure out while italic shorthand prop is not working

const Text = React.forwardRef(
  (
    {
      hidden,
      align,
      decoration,
      transform,
      overflow,
      underline,
      center,
      justify,
      uppercase,
      lowercase,
      capitalize,
      weight,
      bold,
      italic,
      size,
      truncate,
      strokeWidth,
      strokeColor,
      // children,
      ...props
    },
    ref
  ) => {
    //mapProps or something more elegant:
    let style = null
    if (!!uppercase) {
      transform = 'uppercase'
    }
    if (!!lowercase) {
      transform = 'lowercase'
    }
    if (!!capitalize) {
      transform = 'capitalize'
    }
    if (!!underline) {
      decoration = 'underline'
    }
    if (!!center) {
      align = 'center'
    }
    if (!!justify) {
      align = 'justify'
    }
    if (!!bold) {
      weight = 'bold'
    }
    if (!!italic) {
      style = 'italic'
    }

    const styles = { variant: `text.sizes.${size}` }
    if (hidden) {
      styles = { ...styles, ...hideVisually }
    }
    if (truncate && truncate > 0) {
      // styles.textOverflow = 'ellipsis'
      styles.overflow = 'hidden'
      styles.display = '-webkit-box'
      // styles.whiteSpace = 'nowrap'
      styles['-webkit-line-clamp'] = `${truncate}`
      styles['-webkit-box-orient'] = 'vertical'
      // return (
      //   <ChakraText
      //     sx_={hidden && { ...hideVisually() }}
      //     sx__={{ variant: `text.sizes.${size}` }}
      //     sx={styles}
      //     textAlign={align}
      //     textDecoration={decoration}
      //     textTransform={transform}
      //     textOverflow={overflow}
      //     fontWeight={weight}
      //     {...props}
      //   >
      //     <Truncate lines={truncate}>{children}</Truncate>
      //   </ChakraText>
      // )
    }

    if (strokeWidth) {
      styles['-webkit-text-stroke-width'] = strokeWidth
      styles['-webkit-text-stroke-color'] = strokeColor
    }

    return (
      <TextBase
        ref={ref}
        sx={styles}
        textAlign={align}
        textDecoration={decoration}
        textTransform={transform}
        textOverflow={overflow}
        fontWeight={weight}
        fontStyle={style}
        {...props}
      />
    )
  }
)

Text.defaultProps = {
  // as: 'span',
  hidden: false,
  truncate: false,
}

const Heading = React.forwardRef((props, ref) => (
  <Text ref={ref} {...props} sx={{ variant: 'text.headings' }} />
))
Heading.defaultProps = { as: 'h2', size: 'lg' } //default indstad lg
Heading.displayName = 'Text.Heading'

Text.Heading = Heading
Text.displayName = 'Text'

// const Truncate = props => (
//   <Text
//     {...props}
//     overflow="hidden"
//     whiteSpace="nowrap"
//     textOverflow="ellipsis"
//   />
// )
// Text.Truncate = Truncate

//TODO:shortcut props like bold, underlined, uppercase, etc
// const Text = styled(Box)(
//   {},
//   compose(
//     typography,
//     textDecoration,
//     textTransform
//   )
// )

// Text.label = Text.withComponent('label')
// Text.span = Text.withComponent('span')
// Text.p = Text.withComponent('p')
// Text.s = Text.withComponent('s')

export default Text
