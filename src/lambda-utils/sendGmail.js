import nodemailer from 'nodemailer'

/**
     {
            from, //
            to,
            cc,
            bcc,
            subject,
            text
        }
 */
async function sendGmail(envelope) {
  const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secture: true,
    auth: {
      type: 'OAuth2',
      user: envelope.from,
      serviceClient: process.env.GMAIL_CLIENT_ID,
      privateKey: process.env.GMAIL_PRIVATE_KEY,
    },
  })
  try {
    console.log('verifying')
    await transporter.verify((error, success) => console.error(error))
    console.log('sending')
    await transporter.sendMail(envelope)
  } catch (err) {
    console.error(err)
  }
}

export default sendGmail
