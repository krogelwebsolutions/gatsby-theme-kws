import fetch from 'node-fetch'

const URL = 'https://www.google.com/recaptcha/api/siteverify'
const secret = process.env.GRECAPTCHA_SECRET_KEY

const verifyGrecaptcha = async token => {
  console.log('verifying grecaptcha token')
  // const response = await fetch(URL, {
  //   method: 'POST',
  //   body: { secret, response: token },
  // })
  const response = await fetch(URL, {
    method: 'POST',
    body: `secret=${secret}&response=${token}`,
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
  })
  const json = await response.json()
  console.log(json, '*response*')
  return json
}

export default verifyGrecaptcha
