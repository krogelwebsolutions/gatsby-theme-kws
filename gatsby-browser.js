import React from 'react'
import { wrapRoot, wrapPage } from 'lib/Layout/TransitionContext'

//TODO:move this to Layout instead of putting in all of the gatsby config files
export const wrapRootElement = wrapRoot

export const wrapPageElement = wrapPage

//TODO:drive this from config so you can do from end-project
export const onClientEntry = args => {
  if (process.env.NODE_ENV !== 'production') {
    const whyDidYouRender = require('@welldone-software/why-did-you-render')
    //  whyDidYouRender(React, { logOnDifferentValues: false })
  }
}
