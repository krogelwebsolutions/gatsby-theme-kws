// import fs from 'fs'
// import path from 'path'
// import mkdirp from 'mkdirp'

const fs = require('fs')
const path = require('path')
const mkdirp = require('mkdirp')

/*
  Runs before Gatsby does anything
*/
exports.onPreBootstrap = ({ store }, themeOptions) => {
  const { program } = store.getState()

  const contentPath = themeOptions.contentPath || `content`
  const contentDirs = [path.join(program.directory, contentPath)]
  contentDirs.forEach(dir => {
    if (!fs.existsSync(dir)) {
      mkdirp.sync(dir)
    }
  })

  const assetsPath = themeOptions.assetsPath || `assets`
  const assetsDirs = [path.join(program.directory, assetsPath)]
  assetsDirs.forEach(dir => {
    if (!fs.existsSync(dir)) {
      mkdirp.sync(dir)
    }
  })
}

exports.onCreateWebpackConfig = ({ stage, getConfig, actions, store }) => {
  const { program } = store.getState()
  const config = getConfig()

  const aliases = {
    ...config.resolve.alias,
    // rebass: '@rebass/emotion',
  }

  //   if (stage.startsWith('develop')) {
  //     aliases['react-dom'] = '@hot-loader/react-dom' //TODOwhen can we remove this?
  //   }

  // aliases['data'] = path.resolve(__dirname, 'data') //todo: this can be removed after we start using gatsby image
  // aliases['assets'] = path.resolve(__dirname, 'assets')

  actions.setWebpackConfig({
    resolve: {
      alias: aliases,
      modules: [
        path.resolve(__dirname, 'src'),
        path.resolve(program.directory, 'src'),
        'node_modules',
      ],
    },
  })
}
